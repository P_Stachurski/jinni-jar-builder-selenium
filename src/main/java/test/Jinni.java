package test;

import com.github.javafaker.Faker;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static io.github.bonigarcia.wdm.DriverManagerType.FIREFOX;


public class Jinni {

    //setting up driver
    public static WebDriver driver;

    //setting up date
    public static String date = Calendar.getInstance().getTime().toString().replace(':', '-').replace(' ', '_');

    //my methods
    public static void clickWhenReady(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
        element.click();
    }

    public static WebElement getWhenVisible(By locator) {
        WebElement element = null;
        WebDriverWait wait = new WebDriverWait(driver, 10);
        element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return element;
    }

    public static boolean isElementPresent(By locator) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void main(String[] args) throws InterruptedException {

        String driverName = args[0];

        //setting up faker
        Faker faker = new Faker();
        String emailAddress = faker.internet().emailAddress();
        String phoneNumber = faker.number().digits(9);
        String password = faker.internet().password();
        int prefix = faker.number().numberBetween(2, 155);
        String name = faker.name().firstName();
        String surname = faker.name().lastName();
        int gender = faker.number().numberBetween(1, 2);
        String address = faker.address().streetName();
        String city = faker.address().cityName();
        String postCode = faker.address().zipCode();
        int dayOfBirth = faker.number().numberBetween(1, 31);
        int monthOfBirth = faker.number().numberBetween(1, 12);
        int yearOfBirth = faker.number().numberBetween(1901, 2001);
        int currency = faker.number().numberBetween(1, 3);

        //setting up webdriver
        if (driverName.equals("firefox")) {
            WebDriverManager.getInstance(FIREFOX).setup();
            WebDriverManager.firefoxdriver().setup();
            FirefoxProfile profile = new FirefoxProfile();
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setProfile(profile);
            driver = new FirefoxDriver(firefoxOptions);
        } else if (driverName.equals("firefox-headless")) {
            WebDriverManager.getInstance(FIREFOX).setup();
            WebDriverManager.firefoxdriver().setup();
            FirefoxProfile profile = new FirefoxProfile();
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setProfile(profile);
            firefoxOptions.setHeadless(true);
            driver = new FirefoxDriver(firefoxOptions);
        } else if (driverName.equals("chrome")) {
            WebDriverManager.getInstance(CHROME).setup();
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else if (driverName.equals("chrome-headless")) {
            WebDriverManager.getInstance(CHROME).setup();
            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--headless");
            driver = new ChromeDriver(chromeOptions);
        }

        try {
            File file = new File("TestData.txt");
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write("\n");
            fileWriter.write("Browser: " + driverName + "   " + date + ":  " + emailAddress + ", " + phoneNumber + ", " + password + ", " + name + ", " + surname + ", " + address + ", " + city + ", " + postCode + ", " + dayOfBirth + "." + monthOfBirth + "." + yearOfBirth + ", ");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //go to main page
        driver.manage().window().maximize();
        driver.get("https://stage.jinnilotto.com/");

        //click sign up
        clickWhenReady(By.cssSelector(".orange"));

        //fill up 1st register form
        getWhenVisible(By.cssSelector("#registration_email")).sendKeys(emailAddress);
        getWhenVisible(By.cssSelector("#register_phone")).sendKeys(phoneNumber);
        getWhenVisible(By.cssSelector("#registration_password")).sendKeys(password);
        clickWhenReady(By.xpath("(//div[@class='cs-placeholder'])[2]"));
        if (driverName.equals("chrome") || driverName.equals("chrome-headless")) {
            clickWhenReady(By.cssSelector(".open > li:nth-child(2)"));
        } else {
            clickWhenReady(By.cssSelector(".open > li:nth-child(" + prefix + ")"));
        }

        //check if inserted values are correct
        String emailAddressValue = getWhenVisible(By.cssSelector("#registration_email")).getAttribute("value");
        String phoneNumberValue = getWhenVisible(By.cssSelector("#register_phone")).getAttribute("value");
        String passwordValue = getWhenVisible(By.cssSelector("#registration_password")).getAttribute("value");
        Assert.assertTrue(emailAddress.equals(emailAddressValue) && phoneNumber.equals(phoneNumberValue) && password.equals(passwordValue));

        //click continue
        clickWhenReady(By.cssSelector("button.przycisk:nth-child(3)"));

        //fill up 2nd registration form
        getWhenVisible(By.cssSelector("#registration_name")).sendKeys(name);
        getWhenVisible(By.cssSelector("#registration_surname")).sendKeys(surname);

        //random gender
        if (gender == 1) {
            clickWhenReady(By.cssSelector("#registration_gender_male"));
            isElementPresent(By.xpath("//label[@class='genderSelector male success']"));
        } else {
            clickWhenReady(By.cssSelector("#registration_gender_female"));
            isElementPresent(By.xpath("//label[@class='genderSelector female success']"));
        }

//        //choose birth date from a list
        if (driverName.equals("chrome") || driverName.equals("chrome-headless")) {
            clickWhenReady(By.cssSelector("#registration_birthday_2"));
            clickWhenReady(By.xpath("//li[@value='2']"));
            clickWhenReady(By.cssSelector("#registration_birthmonth_2"));
            clickWhenReady(By.xpath("(//li[@value='2'])[2]"));
            clickWhenReady(By.cssSelector("#registration_birthyear_3"));
            clickWhenReady(By.xpath("//li[@value='2000']"));
        } else {
            clickWhenReady(By.cssSelector("#registration_birthday_2"));
            clickWhenReady(By.xpath("//li[@value='" + dayOfBirth + "']"));
            clickWhenReady(By.cssSelector("#registration_birthmonth_2"));
            clickWhenReady(By.xpath("(//li[@value='" + monthOfBirth + "'])[2]"));
            clickWhenReady(By.cssSelector("#registration_birthyear_3"));
            clickWhenReady(By.xpath("//li[@value='" + yearOfBirth + "']"));
        }

        //fill up 2nd registration form cd.
        getWhenVisible(By.cssSelector("#registration_address")).sendKeys(address);
        getWhenVisible(By.cssSelector("#registration_city")).sendKeys(city);
        getWhenVisible(By.cssSelector("#registration_postcode")).sendKeys(postCode);

        //check if inserted values are correct
        String nameValue = getWhenVisible(By.cssSelector("#registration_name")).getAttribute("value");
        String surnameValue = getWhenVisible(By.cssSelector("#registration_surname")).getAttribute("value");
        String addressValue = getWhenVisible(By.cssSelector("#registration_address")).getAttribute("value");
        String cityValue = getWhenVisible(By.cssSelector("#registration_city")).getAttribute("value");
        String postCodeValue = getWhenVisible(By.cssSelector("#registration_postcode")).getAttribute("value");
        Assert.assertTrue((name.equals(nameValue)) && (surname.equals(surnameValue)) && (address.equals(addressValue)) && (city.equals(cityValue)) && (postCode.equals(postCodeValue)));

        //select currency
        clickWhenReady(By.cssSelector("#currency"));
        clickWhenReady(By.cssSelector(".open > li:nth-child(" + currency + ")"));

        //accept terms of use
        clickWhenReady(By.xpath("//label[contains(@for,'termsAndConditions')]"));
        isElementPresent(By.xpath("//label[@class='termsAndConditions currentStep_2 success']"));

        //click on <COMPLETE REGISTRATION>
        clickWhenReady(By.cssSelector("button.przycisk:nth-child(3)"));

        //check if register was successful
        isElementPresent(By.cssSelector(".nameWithHi"));

        try {
            File file = new File("TestData.txt");
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write("   -test passed");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        driver.quit();
    }
}