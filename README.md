Test poprawnej rejestracji na https://stage.jinnilotto.com/

Test napisany jest w jave i selenium framework. Za każdym razem w tescie używane są losowe dane, z wyjątkiem chroma gdzie prefix i data urodzenia nie są generowane losowo.
Dane testowe zbierane są w generowanym pliku TestData(jeśli test wykona się poprawnie na końcu dodawany jest dopisek "-test passed")
Projekt można zbudować do pliku .jar za pomocą taska w Gradle 'jar'

Zbudowany do pliku .jar projekt można uruchomić za pomocą komendy
java -jar jinni-register.jar nazwa_przeglądarki

Obsługiwane przeglądarki: chrome, chrome-headless, firefox, firefox-headless